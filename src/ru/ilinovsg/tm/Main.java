package ru.ilinovsg.tm;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(new URI("https://api.chucknorris.io/jokes/categories")).GET().build();
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        System.out.println(httpResponse.statusCode());

        String str = httpResponse.body().toString();
        str = str.substring(1, str.length() - 1);
        List<String> list = Stream.of(str.split(","))
                .map (elem -> new String(elem))
                .collect(Collectors.toList());

        for (String elem : list) {
            httpRequest = HttpRequest.newBuilder().uri(new URI("https://api.chucknorris.io/jokes/random?"+elem.substring(1, elem.length()-1))).GET().build();
            httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            String str1 = httpResponse.body().toString();
            str1 = str1.substring(1, str1.length() - 1);
            Integer i = str1.lastIndexOf("value");
            System.out.println(str1.substring(i+7, str1.length()));
        }
    }
}

